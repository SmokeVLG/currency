package com.maxden.currency.common.presentation

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.maxden.currency.R
import com.maxden.currency.common.presentation.model.UICurrency
import com.maxden.currency.databinding.RecyclerViewCurrencyItemBinding

class CurrencyListAdapter(private val onClickToFavoriteItem: ((UICurrency) -> Unit)? = null) :
    ListAdapter<UICurrency, CurrencyListAdapter.CurrencyListViewHolder>(ITEM_COMPARATOR) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CurrencyListViewHolder {
        val binding = RecyclerViewCurrencyItemBinding
            .inflate(LayoutInflater.from(parent.context), parent, false)
        return CurrencyListViewHolder(binding
        ) {
            onClickToFavoriteItem?.invoke(it)
        }
    }

    override fun onBindViewHolder(holder: CurrencyListViewHolder, position: Int) {
        val item: UICurrency = getItem(position)
        holder.bind(item)
    }

    inner class CurrencyListViewHolder(
        private val binding: RecyclerViewCurrencyItemBinding,
        private val listener: ((UICurrency) -> Unit)? = null
    ) : RecyclerView.ViewHolder(binding.root) {

        fun bind(item: UICurrency) {
            binding.name.text = item.name
            binding.price.text = item.price.toString()
            if (item.isFavorite) {
                binding.heart.setImageResource(R.drawable.ic_heart_red)
            } else {
                binding.heart.setImageResource(R.drawable.ic_heart)
            }
            binding.heart.setOnClickListener {
                item.isFavorite = !item.isFavorite
                listener?.invoke(item);
            }
        }
    }
}

private val ITEM_COMPARATOR = object : DiffUtil.ItemCallback<UICurrency>() {
    override fun areItemsTheSame(oldItem: UICurrency, newItem: UICurrency): Boolean {
        return oldItem.name == newItem.name
    }

    override fun areContentsTheSame(oldItem: UICurrency, newItem: UICurrency): Boolean {
        return oldItem == newItem
    }
}
