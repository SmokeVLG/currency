package com.maxden.currency.common.domain.repositories

import com.maxden.currency.common.domain.model.currency.Currency
import com.maxden.currency.common.domain.model.pagination.CurrencyList
import com.maxden.currency.sort.domain.model.SearchParameters
import com.maxden.currency.sort.domain.model.SearchResults
import io.reactivex.Flowable

interface CurrencyRepository {
    fun getCurrencyList(): Flowable<List<Currency>>
    suspend fun requestRemoteCurrencyList(from: String): CurrencyList
    suspend fun storeCurrencyList(currencies: List<Currency>)
    suspend fun storeCurrencyItem(currencies: Currency)
    fun searchCachedCurrencyListBy(searchParameters: SearchParameters): Flowable<SearchResults>
}