package com.maxden.currency.common.data.api

import com.maxden.currency.common.data.api.model.ApiCurrencyList
import retrofit2.http.GET
import retrofit2.http.Query

interface CurrencyApi {
    @GET(ApiConstants.ANIMALS_ENDPOINT)
    suspend fun getCurrencyList(
        @Query(ApiParameters.FROM) from: String,
        @Query(ApiParameters.API_KEY) apiKey: String
    ): ApiCurrencyList

    @GET(ApiConstants.ANIMALS_ENDPOINT)
    suspend fun searchCurrencyListBy(
        @Query(ApiParameters.NAME) name: String,
    ): ApiCurrencyList
}
