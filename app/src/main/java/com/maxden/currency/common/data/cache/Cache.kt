package com.maxden.currency.common.data.cache

import com.maxden.currency.common.data.cache.model.cachedcurrency.CachedCurrency
import io.reactivex.Flowable

interface Cache {
    fun getAllCurrencyList(): Flowable<List<CachedCurrency>>
    suspend fun storeListCurrencyList(currencies: List<CachedCurrency>)
    suspend fun storeCurrencyItem(currency: CachedCurrency)
    fun searchCurrencyListBy(
        name: String,
    ): Flowable<List<CachedCurrency>>
}