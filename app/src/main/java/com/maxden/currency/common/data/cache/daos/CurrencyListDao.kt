package com.maxden.currency.common.data.cache.daos

import androidx.room.*
import com.maxden.currency.common.data.cache.model.cachedcurrency.CachedCurrency
import com.maxden.currency.common.data.cache.model.cachedcurrency.CachedCurrencyWithDetails
import io.reactivex.Flowable

@Dao
abstract class CurrencyListDao {
    @Transaction
    @Query("SELECT * FROM currency_list ORDER BY name DESC")
    abstract fun getAllCurrencyList(): Flowable<List<CachedCurrency>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract suspend fun insertCurrencyAggregate(
        currency: CachedCurrencyWithDetails
    )

    suspend fun insertCurrencyListWithDetails(currencies: List<CachedCurrency>) {
        for (animalAggregate in currencies) {
            insertCurrencyAggregate(
                animalAggregate.currency
            )
        }
    }

    suspend fun insertCurrencyItem(currency: CachedCurrency) {
        insertCurrencyAggregate(currency.currency)
    }

    @Transaction
    @Query(
        """
    SELECT * FROM currency_list
      WHERE name LIKE '%' || :name || '%' """
    )
    abstract fun searchCurrencyListBy(name: String): Flowable<List<CachedCurrency>>
}
