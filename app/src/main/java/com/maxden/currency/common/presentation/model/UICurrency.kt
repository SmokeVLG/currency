package com.maxden.currency.common.presentation.model

data class UICurrency(
    val name: String,
    val price: Float?,
    var isFavorite: Boolean
)