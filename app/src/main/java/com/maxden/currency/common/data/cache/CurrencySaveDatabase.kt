package com.maxden.currency.common.data.cache

import androidx.room.Database
import androidx.room.RoomDatabase
import com.maxden.currency.common.data.cache.daos.CurrencyListDao
import com.maxden.currency.common.data.cache.model.cachedcurrency.CachedCurrencyWithDetails

@Database(
    entities = [
        CachedCurrencyWithDetails::class,
    ],
    version = 1
)
abstract class CurrencySaveDatabase : RoomDatabase() {
    abstract fun animalsDao(): CurrencyListDao
}