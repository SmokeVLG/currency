package com.maxden.currency.common.data.api.model

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class ApiCurrencyList(
    @field:Json(name = "base") val baseCurrency: String?,
    @field:Json(name = "results") val currencyList: List<ApiCurrency>?
)

@JsonClass(generateAdapter = true)
data class ApiCurrency(
    @field:Json(name = "name") val name: String?,
    @field:Json(name = "value") val value: Float?
)