package com.maxden.currency.common.data

import com.maxden.currency.common.data.api.ApiConstants
import com.maxden.currency.common.data.api.CurrencyApi
import com.maxden.currency.common.data.api.model.mappers.ApiCurrencyMapper
import com.maxden.currency.common.data.cache.Cache
import com.maxden.currency.common.data.cache.model.cachedcurrency.CachedCurrency
import com.maxden.currency.common.domain.model.NetworkException
import com.maxden.currency.common.domain.model.currency.Currency
import com.maxden.currency.common.domain.model.pagination.CurrencyList
import com.maxden.currency.common.domain.repositories.CurrencyRepository
import com.maxden.currency.sort.domain.model.SearchParameters
import com.maxden.currency.sort.domain.model.SearchResults
import io.reactivex.Flowable
import retrofit2.HttpException
import javax.inject.Inject

class CurrencyRepository @Inject constructor(
    private val api: CurrencyApi,
    private val cache: Cache,
    private val apiCurrencyMapper: ApiCurrencyMapper,
) : CurrencyRepository {

    override fun getCurrencyList(): Flowable<List<Currency>> {
        return cache.getAllCurrencyList()
            //Проверяет есть ли измененные данные в локальной базе данных ?
            //Если есть - отправляем данные в слой домен
            .distinctUntilChanged()
            .map { currencyList ->
                currencyList.map {
                    it.currency.toCurrencyDomain()
                }
            }
    }

    override suspend fun requestRemoteCurrencyList(from: String): CurrencyList {
        try {
            val (baseCurrency, allCurrency) = api.getCurrencyList(from, ApiConstants.KEY)
            return CurrencyList(
                allCurrency?.map { apiCurrencyMapper.mapToDomain(it) }.orEmpty()
            )
        } catch (exception: HttpException) {
            throw NetworkException(exception.message ?: "Code ${exception.code()}")
        }
    }

    override suspend fun storeCurrencyList(currencies: List<Currency>) {
        cache.storeListCurrencyList(currencies.map { CachedCurrency.fromDomain(it) })
    }

    override suspend fun storeCurrencyItem(currency: Currency) {
        cache.storeCurrencyItem(CachedCurrency.fromDomain(currency))
    }


    override fun searchCachedCurrencyListBy(searchParameters: SearchParameters): Flowable<SearchResults> {
        val (name) = searchParameters
        return cache.searchCurrencyListBy(
            name
        ).distinctUntilChanged().map { currencyList ->
            currencyList.map {
                it.currency.toCurrencyDomain()
            }
        }
            .map { SearchResults(it, searchParameters) }
    }
}