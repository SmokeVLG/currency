package com.maxden.currency.common.data.api.model.mappers

import com.maxden.currency.common.data.api.model.ApiCurrency
import com.maxden.currency.common.domain.model.currency.Currency
import javax.inject.Inject

class ApiCurrencyMapper @Inject constructor(
) : ApiMapper<String, Currency> {
    override fun mapToDomain(apiEntity: ApiCurrency?): Currency {
        return Currency(
            name = apiEntity?.name ?: "",
            price = apiEntity?.value,
            isFavorite = false
        )
    }
}
