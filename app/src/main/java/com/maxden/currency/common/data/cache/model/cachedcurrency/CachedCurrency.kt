package com.maxden.currency.common.data.cache.model.cachedcurrency

import androidx.room.Embedded
import com.maxden.currency.common.domain.model.currency.Currency

data class CachedCurrency(
    @Embedded
    val currency: CachedCurrencyWithDetails,
) {
    companion object {
        fun fromDomain(currency: Currency): CachedCurrency {
            return CachedCurrency(
                currency = CachedCurrencyWithDetails.fromDomain(currency),
            )
        }
    }
}
