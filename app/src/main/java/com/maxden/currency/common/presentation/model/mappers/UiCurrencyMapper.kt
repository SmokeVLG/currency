package com.maxden.currency.common.presentation.model.mappers

import com.maxden.currency.common.domain.model.currency.Currency
import com.maxden.currency.common.presentation.model.UICurrency
import javax.inject.Inject

class UiCurrencyMapper @Inject constructor() : UiMapper<Currency, UICurrency> {
    override fun mapToView(input: Currency): UICurrency {
        return UICurrency(
            name = input.name,
            price = input.price,
            isFavorite = input.isFavorite
        )
    }
}
