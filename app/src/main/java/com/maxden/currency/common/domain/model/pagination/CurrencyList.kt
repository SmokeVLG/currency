package com.maxden.currency.common.domain.model.pagination

import com.maxden.currency.common.domain.model.currency.Currency

data class CurrencyList(
    val currencies: List<Currency>
)