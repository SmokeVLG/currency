package com.maxden.currency.common.data.cache

import com.maxden.currency.common.data.cache.daos.CurrencyListDao
import com.maxden.currency.common.data.cache.model.cachedcurrency.CachedCurrency
//import com.maxden.currency.common.data.cache.model.cachedorganization.CachedOrganization
import io.reactivex.Flowable
import javax.inject.Inject

class RoomCache @Inject constructor(
    private val currencyListDao: CurrencyListDao,
) : Cache {
    override fun getAllCurrencyList(): Flowable<List<CachedCurrency>> {
        return currencyListDao.getAllCurrencyList()
    }

    override suspend fun storeListCurrencyList(currencies: List<CachedCurrency>) {
        currencyListDao.insertCurrencyListWithDetails(currencies)
    }

    override suspend fun storeCurrencyItem(currency: CachedCurrency){
        currencyListDao.insertCurrencyItem(currency)
    }

    override fun searchCurrencyListBy(
        name: String
    ): Flowable<List<CachedCurrency>> {
        return currencyListDao.searchCurrencyListBy(
            name
        )
    }
}