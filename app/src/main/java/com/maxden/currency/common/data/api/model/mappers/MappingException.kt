package com.maxden.currency.common.data.api.model.mappers

class MappingException(message: String) : Exception(message)