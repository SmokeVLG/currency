package com.maxden.currency.common.domain.model.currency

data class Currency(
    val name: String,
    val price: Float?,
    val isFavorite: Boolean,
)