package com.maxden.currency.common.data.di

import android.content.Context
import androidx.room.Room
import com.maxden.currency.common.data.cache.Cache
import com.maxden.currency.common.data.cache.CurrencySaveDatabase
import com.maxden.currency.common.data.cache.RoomCache
import com.maxden.currency.common.data.cache.daos.CurrencyListDao
import dagger.Binds
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
abstract class CacheModule {
    @Binds
    abstract fun bindCache(cache: RoomCache): Cache

    companion object {
        @Provides
        @Singleton
        fun provideDatabase(
            @ApplicationContext context: Context
        ): CurrencySaveDatabase {
            return Room.databaseBuilder(
                context,
                CurrencySaveDatabase::class.java,
                "currencysave.db"
            )
                .build()
        }

        @Provides
        fun provideAnimalsDao(
            currencySaveDatabase: CurrencySaveDatabase
        ): CurrencyListDao = currencySaveDatabase.animalsDao()
    }
}