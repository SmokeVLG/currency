package com.maxden.currency.common.data.api

object ApiConstants {
    const val BASE_ENDPOINT = "http://10.0.2.2:3000/"
    const val ANIMALS_ENDPOINT = "fetch-all"
    const val KEY = "f8dc61bc47-4f7dde550c-racc1j"
}

object ApiParameters {
    const val NAME = "name"
    const val FROM = "from"
    const val API_KEY = "api_key"
}