package com.maxden.currency.common.data.cache.model.cachedcurrency

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.maxden.currency.common.domain.model.currency.Currency

@Entity(
    tableName = "currency_list",
)
data class CachedCurrencyWithDetails(
    @PrimaryKey
    val name: String,
    val price: Float?,
    val isFavorite: Boolean,
) {
    companion object {
        fun fromDomain(domainModel: Currency): CachedCurrencyWithDetails {
            return CachedCurrencyWithDetails(
                name = domainModel.name,
                price = domainModel.price,
                isFavorite =  domainModel.isFavorite
            )
        }
    }

    fun toCurrencyDomain(): Currency {
        return Currency(
            name = name,
            price = price,
            isFavorite = isFavorite
        )
    }

}
