package com.maxden.currency.common.data.api.model.mappers

import com.maxden.currency.common.data.api.model.ApiCurrency

interface ApiMapper<E, D> {
    fun mapToDomain(apiEntity: ApiCurrency?): D
}