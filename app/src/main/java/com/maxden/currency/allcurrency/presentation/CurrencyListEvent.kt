package com.maxden.currency.allcurrency.presentation

sealed class CurrencyListEvent {
    object RequestRemoteCurrencyList : CurrencyListEvent()
}