package com.maxden.currency.allcurrency.presentation

import com.maxden.currency.common.presentation.Event
import com.maxden.currency.common.presentation.model.UICurrency

data class CurrencyListViewState(
    val loading: Boolean = true,
    val currencies: List<UICurrency> = emptyList(),
    val failure: Event<Throwable>? = null
)