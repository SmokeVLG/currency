package com.maxden.currency.allcurrency.domain.usecases

import com.maxden.currency.common.domain.model.currency.Currency
import com.maxden.currency.common.domain.repositories.CurrencyRepository
import javax.inject.Inject

class SaveCurrencyToFavorite @Inject constructor(private val currencyRepository: CurrencyRepository) {
    suspend operator fun invoke(currency: Currency) {
        currencyRepository.storeCurrencyItem(currency)
    }
}