package com.maxden.currency.allcurrency.domain.usecases

import com.maxden.currency.common.domain.model.currency.Currency
import com.maxden.currency.common.domain.repositories.CurrencyRepository
import javax.inject.Inject

class RequestRemoteCurrencyList @Inject constructor(private val currencyRepository: CurrencyRepository) {
    suspend operator fun invoke(from: String): List<Currency> {
        val (currencyList) = currencyRepository.requestRemoteCurrencyList(from)
        currencyRepository.storeCurrencyList(currencyList)
        return currencyList
    }
}