package com.maxden.currency.allcurrency.presentation

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.maxden.currency.allcurrency.domain.usecases.GetCurrencyList
import com.maxden.currency.allcurrency.domain.usecases.RequestRemoteCurrencyList
import com.maxden.currency.allcurrency.domain.usecases.SaveCurrencyToFavorite
import com.maxden.currency.common.domain.model.NetworkException
import com.maxden.currency.common.domain.model.NetworkUnavailableException
import com.maxden.currency.common.domain.model.currency.Currency
import com.maxden.currency.common.presentation.Event
import com.maxden.currency.common.presentation.model.UICurrency
import com.maxden.currency.common.presentation.model.mappers.UiCurrencyMapper
import com.maxden.currency.common.utils.DispatchersProvider
import com.maxden.currency.common.utils.createExceptionHandler
import com.maxden.logging.Logger
import dagger.hilt.android.lifecycle.HiltViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.addTo
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import javax.inject.Inject

@HiltViewModel
class CurrencyListFragmentViewModel @Inject constructor(
    private val getCurrencyList: GetCurrencyList,
    private val requestRemoteCurrencyList: RequestRemoteCurrencyList,
    private val saveCurrencyToFavorite: SaveCurrencyToFavorite,
    private val uiCurrencyMapper: UiCurrencyMapper,
    private val dispatchersProvider: DispatchersProvider,
    private val compositeDisposable: CompositeDisposable
) : ViewModel() {
    val state: LiveData<CurrencyListViewState> get() = _state
    var isLoadingCurrencyList: Boolean = false
    private val _state = MutableLiveData<CurrencyListViewState>()

    init {
        _state.value = CurrencyListViewState()
        subscribeToCurrencyUpdates()
    }

    fun onEvent(event: CurrencyListEvent) {
        when (event) {
            is CurrencyListEvent.RequestRemoteCurrencyList -> loadRemoteCurrencyList()
        }
    }

    private fun subscribeToCurrencyUpdates() {
        getCurrencyList()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                { onNewCurrencyList(it) },
                { onFailure(it) }
            )
            .addTo(compositeDisposable)
    }

    private fun onNewCurrencyList(currencies: List<Currency>) {
        Logger.d("Got more currency!")

        val currencyList = currencies.map { uiCurrencyMapper.mapToView(it) }
        _state.value = state.value!!.copy(loading = false, currencies = currencyList)
    }

    private fun loadRemoteCurrencyList() {
        isLoadingCurrencyList = true


        val errorMessage = "Failed to fetch currency list"
        val exceptionHandler = viewModelScope.createExceptionHandler(errorMessage) { onFailure(it) }

        viewModelScope.launch(exceptionHandler) {
            withContext(dispatchersProvider.io()) {
                Logger.d("Requesting currency list.")
                requestRemoteCurrencyList(from = "USD")
            }

            isLoadingCurrencyList = false
        }
    }

    private fun onFailure(failure: Throwable) {
        when (failure) {
            is NetworkException,
            is NetworkUnavailableException -> {
                _state.value = state.value!!.copy(
                    loading = false,
                    failure = Event(failure)
                )
            }
        }
    }

    override fun onCleared() {
        super.onCleared()
        compositeDisposable.clear()
    }

    fun saveCurrencyItem(uiCurrency: UICurrency) {
        viewModelScope.launch(Dispatchers.IO) {
            withContext(dispatchersProvider.io()) {
                saveCurrencyToFavorite.invoke(
                    Currency(
                        uiCurrency.name,
                        uiCurrency.price,
                        uiCurrency.isFavorite
                    )
                )
            }
        }
    }
}
