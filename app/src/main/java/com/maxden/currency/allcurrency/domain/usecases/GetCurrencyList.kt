package com.maxden.currency.allcurrency.domain.usecases

import com.maxden.currency.common.domain.repositories.CurrencyRepository
import javax.inject.Inject

class GetCurrencyList @Inject constructor(private val currencyRepository: CurrencyRepository) {
    operator fun invoke() = currencyRepository.getCurrencyList()
        .filter { it.isNotEmpty() }
}