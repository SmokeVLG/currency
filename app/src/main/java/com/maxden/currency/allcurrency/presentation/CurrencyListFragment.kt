package com.maxden.currency.allcurrency.presentation

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.GridLayoutManager
import com.google.android.material.snackbar.Snackbar
import com.maxden.currency.R
import com.maxden.currency.common.presentation.CurrencyListAdapter
import com.maxden.currency.common.presentation.Event
import com.maxden.currency.databinding.FragmentAllCurrencyBinding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class CurrencyListFragment : Fragment() {
    companion object {
        private const val ITEMS_PER_ROW = 1
    }

    private val viewModel: CurrencyListFragmentViewModel by viewModels()
    private val binding get() = _binding!!

    private var _binding: FragmentAllCurrencyBinding? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentAllCurrencyBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupUI()
        requestInitialCurrencyList()
    }

    private fun setupUI() {
        val adapter = createAdapter()
        setupRecyclerView(adapter)
        observeViewStateUpdates(adapter)
    }

    private fun createAdapter(): CurrencyListAdapter {
        return CurrencyListAdapter {
            viewModel.saveCurrencyItem(it)
        }
    }

    private fun setupRecyclerView(currencyListAdapter: CurrencyListAdapter) {
        binding.animalsRecyclerView.apply {
            adapter = currencyListAdapter
            layoutManager = GridLayoutManager(requireContext(), ITEMS_PER_ROW)
            setHasFixedSize(true)
        }
    }


    private fun observeViewStateUpdates(adapter: CurrencyListAdapter) {
        viewModel.state.observe(viewLifecycleOwner) {
            updateScreenState(it, adapter)
        }
    }

    private fun updateScreenState(state: CurrencyListViewState, adapter: CurrencyListAdapter) {
        binding.progressBar.isVisible = state.loading
        adapter.submitList(state.currencies)
        handleFailures(state.failure)
    }

    private fun handleFailures(failure: Event<Throwable>?) {
        val unhandledFailure = failure?.getContentIfNotHandled() ?: return

        val fallbackMessage = getString(R.string.an_error_occurred)

        val snackbarMessage = if (unhandledFailure.message.isNullOrEmpty()) {
            fallbackMessage
        } else {
            unhandledFailure.message!!
        }
        if (snackbarMessage.isNotEmpty()) {
            Snackbar.make(requireView(), snackbarMessage, Snackbar.LENGTH_SHORT).show()
        }
    }

    private fun requestInitialCurrencyList() {
        viewModel.onEvent(CurrencyListEvent.RequestRemoteCurrencyList)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}