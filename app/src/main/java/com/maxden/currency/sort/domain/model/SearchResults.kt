package com.maxden.currency.sort.domain.model

import com.maxden.currency.common.domain.model.currency.Currency

data class SearchResults(
    val currencies: List<Currency>,
    val searchParameters: SearchParameters
)