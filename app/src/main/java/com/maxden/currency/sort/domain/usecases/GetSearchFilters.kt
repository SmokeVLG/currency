package com.maxden.currency.sort.domain.usecases

import com.maxden.currency.common.domain.repositories.CurrencyRepository
import com.maxden.currency.sort.domain.model.SearchFilters
import javax.inject.Inject

class GetSearchFilters @Inject constructor(private val currencyRepository: CurrencyRepository) {

    companion object {
        const val NO_FILTER_SELECTED = "Any"
    }

    suspend operator fun invoke(): SearchFilters {
        return SearchFilters(
            // ages, types
        )
    }
}