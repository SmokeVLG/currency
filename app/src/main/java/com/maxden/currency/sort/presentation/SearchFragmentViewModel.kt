package com.maxden.currency.sort.presentation

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.maxden.currency.common.domain.model.currency.Currency
import com.maxden.currency.common.presentation.model.mappers.UiCurrencyMapper
import com.maxden.currency.common.utils.DispatchersProvider
import com.maxden.currency.common.utils.createExceptionHandler
import com.maxden.currency.sort.domain.model.SearchResults
import com.maxden.currency.sort.domain.usecases.GetSearchFilters
import com.maxden.currency.sort.domain.usecases.SearchCurrency
import dagger.hilt.android.lifecycle.HiltViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.addTo
import io.reactivex.subjects.BehaviorSubject
import kotlinx.coroutines.*
import javax.inject.Inject

@HiltViewModel
class SearchFragmentViewModel @Inject constructor(
    //private val searchCurrencyListRemotely: SearchCurrencyListRemotely,
    private val searchCurrency: SearchCurrency,
    private val getSearchFilters: GetSearchFilters,
    private val uiCurrencyMapper: UiCurrencyMapper,
    private val dispatchersProvider: DispatchersProvider,
    private val compositeDisposable: CompositeDisposable
) : ViewModel() {

    val state: LiveData<SearchViewState> get() = _state

    private val _state: MutableLiveData<SearchViewState> = MutableLiveData()
    private val querySubject = BehaviorSubject.create<String>()
    private val ageSubject = BehaviorSubject.createDefault("")
    private val typeSubject = BehaviorSubject.createDefault("")

    private var remoteSearchJob: Job = Job()

    init {
        _state.value = SearchViewState()
    }

    fun onEvent(event: SearchEvent) {
        when (event) {
            is SearchEvent.PrepareForSearch -> prepareForSearch()
            else -> onSearchParametersUpdate(event)
        }
    }

    private fun onSearchParametersUpdate(event: SearchEvent) {
        remoteSearchJob.cancel(
            CancellationException("New search parameters incoming!")
        )

        when (event) {
            is SearchEvent.QueryInput -> updateQuery(event.input)
            is SearchEvent.AgeValueSelected -> updateAgeValue(event.age)
            is SearchEvent.TypeValueSelected -> updateTypeValue(event.type)
        }
    }

    private fun prepareForSearch() {
        loadFilterValues()
        setupSearchSubscription()
    }

    private fun loadFilterValues() {
        val exceptionHandler =
            createExceptionHandler(
                message = "Failed to get filter values!"
            )

        viewModelScope.launch(exceptionHandler) {
            //val (ages, types) = withContext(dispatchersProvider.io()) { getSearchFilters() }

            updateStateWithFilterValues(
                //  ages, types
            )
        }
    }

    private fun createExceptionHandler(message: String): CoroutineExceptionHandler {
        return viewModelScope.createExceptionHandler(message) {
            onFailure(it)
        }
    }

    private fun updateStateWithFilterValues() {
        _state.value = state.value!!.updateToReadyToSearch()
    }

    private fun setupSearchSubscription() {
        searchCurrency(querySubject, ageSubject, typeSubject)
            .observeOn(AndroidSchedulers.mainThread()).subscribe(
                { onSearchResults(it) },
                { onFailure(it) }
            )
            .addTo(compositeDisposable)
    }

    private fun onSearchResults(searchResults: SearchResults) {
        val (animals, searchParameters) = searchResults
        onAnimalList(animals)
    }


    private fun updateQuery(input: String) {
        querySubject.onNext(input)

        if (input.isEmpty()) {
            setNoSearchQueryState()
        } else {
            setSearchingState()
        }
    }

    private fun updateAgeValue(age: String) {
        ageSubject.onNext(age)
    }

    private fun updateTypeValue(type: String) {
        typeSubject.onNext(type)
    }

    private fun setSearchingState() {
        _state.value = state.value!!.updateToSearching()
    }

    private fun setNoSearchQueryState() {
        _state.value = state.value!!.updateToNoSearchQuery()
    }

    private fun onAnimalList(currencies: List<Currency>) {
        _state.value =
            state.value!!.updateToHasSearchResults(currencies.map { uiCurrencyMapper.mapToView(it) })
    }

    private fun onFailure(throwable: Throwable) {
        state.value!!.updateToHasFailure(throwable)
    }

    override fun onCleared() {
        super.onCleared()
        compositeDisposable.clear()
    }
}
