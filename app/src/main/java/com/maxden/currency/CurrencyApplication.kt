package com.maxden.currency

import android.app.Application
import com.maxden.logging.Logger

import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class CurrencyApplication : Application() {
    override fun onCreate() {
        super.onCreate()

        initLogger()
    }

    private fun initLogger() {
        Logger.init()
    }
}